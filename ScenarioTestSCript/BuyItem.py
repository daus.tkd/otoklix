from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from time import sleep
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import random
#from faker import Faker
import unittest, time, re, datetime
import warnings
import os
#from dotenv import load_dotenv


class Scenariotest(unittest.TestCase):
    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        warnings.filterwarnings(action="ignore", message="unclosed", category=ResourceWarning)
        s=Service(r'C:\Users\Admin\Documents\PVG\chromedriver.exe')
        self.driver = webdriver.Chrome(service=s)
        self.driver.implicitly_wait(5)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_BuyItem(self):
        driver = self.driver
        #wait = WebDriverWait(driver, 20)
        driver.get("https://www.saucedemo.com/")
        driver.maximize_window()
        #step 1 Login
        driver.find_element(By.ID, "user-name").click()
        driver.find_element(By.ID, "user-name").clear()
        driver.find_element(By.ID, "user-name").send_keys("standard_user")
        time.sleep(2)
        driver.find_element(By.ID, "password").click()
        driver.find_element(By.ID, "password").clear()
        driver.find_element(By.ID, "password").send_keys("secret_sauce")
        time.sleep(2)
        driver.find_element(By.ID, "login-button").click()
        time.sleep(2)
        #step 2 add to cart a item
        driver.find_element(By.ID, "add-to-cart-sauce-labs-backpack").click()
        time.sleep(2)
        #step 3 go to cart and check out the item
        driver.find_element(By.XPATH, "//div[@id='shopping_cart_container']/a/span").click()
        time.sleep(2)
        driver.find_element(By.ID, "checkout").click()
        time.sleep(2)
        #step 4 fill checkout information
        driver.find_element(By.ID, "first-name").click()
        driver.find_element(By.ID, "first-name").clear()
        driver.find_element(By.ID, "first-name").send_keys("pende")
        time.sleep(2)
        driver.find_element(By.ID, "last-name").click()
        driver.find_element(By.ID, "last-name").clear()
        driver.find_element(By.ID, "last-name").send_keys("alias")
        time.sleep(2)
        driver.find_element(By.ID, "postal-code").click()
        driver.find_element(By.ID, "postal-code").clear()
        driver.find_element(By.ID, "postal-code").send_keys("12345")
        time.sleep(2)
        #step 5 overview and finish order
        driver.find_element(By.ID, "continue").click()
        time.sleep(2)
        try:
            VerifyOrder = self.driver.find_element(By.CLASS_NAME, "inventory_item_name").text
            assert "Sauce Labs Backpack" in VerifyOrder
            print("Assertion is TRUE")

        except Exception as e:
            print("Assertion is FALSE", format(e))
        #step 6 see thank you page order
        driver.find_element(By.ID, "finish").click()
        driver.find_element(By.TAG_NAME, 'body').send_keys(Keys.CONTROL + Keys.HOME)
        time.sleep(2)
        driver.save_screenshot(r'C:\Users\Admin\Documents\otoklix\ScenarioTestSCript\Screenshoot\Capture.png')

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
